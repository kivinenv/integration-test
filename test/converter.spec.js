// TDD - unit testing

const expect = require('chai').expect;
const converter = require('../src/converter')

describe("Color Code Converter", () => {
    describe("Hex to RGB Conversion", () => {
        it("converts the basic colours", () => {
            const redRgb = converter.hexToRgb('ff0000'.match(/.{1,2}/g)); //255,0,0
            const greenRgb = converter.hexToRgb('00ff00'.match(/.{1,2}/g)); // 0,255,0)
            const blueRgb = converter.hexToRgb('0000ff'.match(/.{1,2}/g)); // 0,0,255

            expect(redRgb).to.deep.equal([ 255, 0, 0 ]);
            expect(greenRgb).to.deep.equal([ 0, 255, 0 ]);
            expect(blueRgb).to.deep.equal([ 0, 0, 255 ]);
            
        });
    });
});