module.exports = {
/**
 * 
 * @param {array} hex 
 * @returns {array} rbgvalues in array
 */

    hexToRgb: (hex) => {
        let rgbValues = [parseInt(hex[0], 16), parseInt(hex[1], 16), parseInt(hex[2], 16)]; 
        return rgbValues;
    }
}

