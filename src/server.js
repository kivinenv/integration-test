const { request } = require('express');
const express = require('express');
const res = require('express/lib/response');
const converter = require('./converter');
const app = express();
const port = 3000;

app.get('/', (req, res) => res.send("Hello"));


//http://localhost:3000/hex-to-rgb?hex=xxxxxx
app.get('/hex-to-rgb', (req, res) => {
    const hex = req.query.hex.match(/.{1,2}/g);
    res.send(converter.hexToRgb(hex));
});

if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(port, () => console.log(`Server: localhost:${port}`));
}